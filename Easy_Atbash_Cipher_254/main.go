package main

import (
	"fmt"
	"errors"
	"unicode/utf8"
	"strings"
	"log"
	"os"
)


func cipher(input string) (string, error) {

	ALPHABET := "abcdefghijklmnopqrstuvwxyz"

	if utf8.RuneCountInString(input) == 0 { // Checks if the string is empty
		return "", errors.New("Can't cipher empty strings")
	}

	var output string // The output (Ciphered) string

	for _, c := range input { // Iterates over the string
		sCase := string(c) == strings.ToUpper(string(c)) // Checks if the letter is Uppercase
		i := strings.Index(ALPHABET, string(c))
		reversedI := 25 - i
		if i == -1 {
			if sCase { // IF UpperCase
				output += strings.ToUpper(string(c))
			} else {
				output += string(c)
			}
		} else {
			if sCase { // IF UpperCase
				output += strings.ToUpper(string([]rune(ALPHABET)[reversedI]))
			} else {
				output += string([]rune(ALPHABET)[reversedI])
			}
		}
	}

	return output, nil

}

func main() {
	input := strings.Join(os.Args[1:], " ")
	o, e := cipher(string(input))
	if e != nil {
		log.Fatal(e)
		os.Exit(1)
	}
	fmt.Println(o)
}